// Escreva um algoritmo para ler uma temperatura em graus Fahrenheit, calcular e escrever o valor correspondente em graus Celsius (baseado na fórmula abaixo):
// c/5 = (f-32)/9

// Temperatura (aleatória entre 0 e 45) em Celsius
const temperaturaCelsius = Math.round(Math.random() * 45);

// Função para realizar o cálculo
function paraFahrenheit(temperatura) {return 9 * temperatura / 5 + 32}
// function paraFahrenheit(temperatura) {
//     fahrenheit = 9 * temperatura / 5 + 32;
//     return fahrenheit;
// }

// Criando variável
let temperaturaFahrenheit = paraFahrenheit(temperaturaCelsius);

// Print
console.log(`${temperaturaCelsius}ºC = ${temperaturaFahrenheit}`);
