// Faça um algoritmo em que você recebe 3 notas de um aluno e caso a média aritmética dessas notas for maior ou igual que 6 imprima “Aprovado”, caso contrário “Reprovado”.

// Criando as notas aleatórias (0 a 10)
const notas = [];
for (let i = 0; i < 3; i++) {
    notas[i] = Math.round(Math.random() * 10);
}

// Calculando a média
let media = notas.reduce((s, n) => s + n) / notas.length

// Checando a aprovação:
let aprovado = media >= 6 ? "Aprovado" : "Reprovado"

// Ecrevendo as notas na tela
console.log(`Notas: ${notas}`);

// Escrevendo o resultado
console.log(`Aluno ${aprovado} \nMedia: ${media.toFixed(2)}`);
