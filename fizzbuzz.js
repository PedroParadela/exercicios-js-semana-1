// Teste 5 números inteiros aleatórios. Os testes: 
// Caso o valor seja divisível por 3, imprima no console “fizz”. 
// Caso o valor seja divisível por 5 imprima “buzz”. 
// Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima “fizzbuzz”.
// Caso contrário imprima o número.


// Função para fazer o teste
function teste(numero) {
    let resposta = "";

    if (numero % 3 == 0) {resposta += "fizz"}

    if (numero % 5 == 0) {resposta += "buzz"}

    return resposta == "" ? numero : resposta;
}

// Repetindo 5 vezes para 5 números aleatórios
for (let i = 0; i < 5; i++) {
    let numero = Math.round(Math.random() * 100);
    let resposta = teste(numero);
    console.log(resposta);
}
